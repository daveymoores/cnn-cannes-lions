<?php include("./components/head.php"); ?>

<?php include("./components/navigation.php"); ?>

<div class="hero hero--need-to-know">
    <div class="container">
        <div class="row">
            <div class="col-md-7 col-sm-7 col-xs-6">
                <h2>Need To Know</h2>
            </div>
            <div class="col-md-5 col-sm-5 col-xs-6">
                <img src="dist/images/lions-logo.png" alt="" />
            </div>
        </div>
    </div>
    <div class="hero__panel--blue"></div>
    <div class="hero__panel--body"></div>
</div>

<div class="site-content--wrapper">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3 class="ntk-section__heading">Local Info</h3>

                <div class="row">
                    <div class="col-md-4 col-sm-4">
                        <div class="temperature">
                            <div class="temperature__title">
                                Average Temperature
                            </div>
                            <div class="temperature__temp">
                                <i class="fa fa-sun-o" aria-hidden="true"></i> 68&#8457;/20&#8451;
                            </div>
                        </div>
                    </div>
                    <div class="col-md-7 col-sm-7 col-md-offset-1 col-sm-offset-1">
                        <div class="currency">
                            <div class="currency__title">
                                Currency
                            </div>
                            <div class="currency__currencies" data-widget="currency"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <h3 class="ntk-section__heading">CNN Guest Locations</h3>
                <iframe src="https://www.google.com/maps/d/u/0/embed?mid=18LxUb0hGCY6B5nkzf-KMGrOQEg0" width="640" height="480"></iframe>
            </div>
        </div>

        <div class="row">
            <div class="col-md-7 col-sm-7">
                <h3 class="ntk-section__heading">Shuttle Bus</h3>
                <p class="ntk-section__content">There will be a shuttle bus taking you from the Pullman to the Palais des Festivals and back every 15 minutes.  While we will try our best to stick to the timetable, please make allowances for traffic at peak times.</p>
            </div>
            <div class="col-md-5 col-sm-5">
                <a href="dist/images/CNN-Cannes-Lions-Shuttle-Timetable-A5-3.pdf" download="CNN-Cannes-Lions-Shuttle-Timetable.pdf" class="btn btn-ghost">DOWNLOAD TIMETABLE</a>
            </div>
        </div>

        <div class="row">
            <div class="col-md-7">
                <h3 class="ntk-section__heading">Arrival &amp; Departure Details</h3>
                <p class="ntk-section__content">A car has been arranged to collect you on arrival from Nice Airport to the hotel.  For departure, please be advised that your car will leave the hotel 3 hours before your flight is due to depart. Feel free to visit the CNN Desk in the hotel between 09:00 – 11:00 on Tuesday &amp; Wednesday if you would like to change this.</p>
            </div>
        </div>

        <div class="row">
            <div class="col-md-7">
                <h3 class="ntk-section__heading">Hotel</h3>
                <ul class="ntk-section__content">
                    <li>Pullman Cannes Mandelieu Royal Casino</li>
                    <li>605 Avenue du Général de Gaulle</li>
                    <li>06210 Mandelieu-La Napoule, FRANCE</li>
                    <li>Phone: +33 (0)4 92 97 70 00</li>
                    <li>Website: <a href="http://pullman-mandelieu.com/" target="_blank">http://pullman-mandelieu.com/</a></li>
                </ul>
            </div>
        </div>

        <div class="row">
            <div class="col-md-7">
                <h3 class="ntk-section__heading">Questions</h3>
                <p class="ntk-section__content">If you have any questions in the lead up to the event, please contact the CNN <a href="mailTo:CNNEvents@cnn.com">Events Team</a> or your CNN Sales Representative.</a></p>
            </div>
        </div>

    </div>
</div>

<?php include("./components/social-feed.php"); ?>

<?php include("./components/footer.php"); ?>

<?php include("./components/foot.php"); ?>
