<?php include("./components/head.php"); ?>

<?php include("./components/navigation.php"); ?>

<div class="hero hero--registration">
    <div class="container">
        <div class="row">
            <div class="col-md-7 col-sm-7 col-xs-6">
                <h2>Registration</h2>
            </div>
            <div class="col-md-5 col-sm-5 col-xs-6">
                <img src="dist/images/lions-logo.png" alt="" />
            </div>
        </div>
    </div>
    <div class="hero__panel--blue"></div>
    <div class="hero__panel--body"></div>
</div>


<div class="site-content--wrapper">
    <div class="container">
        <div class="row" data-widget="form-behaviour">

                <form data-toggle="validator" id="registration" class="formtype" accept-charset="UTF-8" action="https://formkeep.com/f/ebb942bc893c" method="POST">
                    <div class="col-md-7 col-sm-7">
                      <input type="hidden" name="utf8" value="✓">
                      <label id="stop-the-bots" for="stop-the-bots">Please leave blank:
                      <input type="text" name="stop-the-bots" id="stop-the-bots">
                      </label>

                      <div class="formtype__section">
                          <h4>Your Details</h4>
                          <div class="form-group">
                              <label for="title">Title<sup>*</sup></label>
                              <div class=formtype__select>
                                  <select id="title" name="title" data-error="Please choose a title." class="form-control" required>
                                      <option value="0">Choose a title...</option>
                                      <option value="mr">Mr</option>
                                      <option value="mrs">Mrs</option>
                                      <option value="ms">Ms</option>
                                      <option value="miss">Miss</option>
                                      <option value="dr">Dr</option>
                                      <option value="prof">Prof</option>
                                      <option value="sir">Sir</option>
                                  </select>
                              </div>
                              <div class="help-block with-errors"></div>
                          </div>

                          <div class="formtype__double">
                              <div class="form-group">
                                  <label for="f_name">First Name<sup>*</sup></label>
                                  <input type="text" class="form-control" name="f_name" id="f_name" placeholder="First name" data-error="Please give your first name." required>
                                  <div class="help-block with-errors"></div>
                              </div>
                              <div class="form-group">
                                  <label for="l_name">Last Name<sup>*</sup></label>
                                  <input type="text" class="form-control" id="l_name" name="l_name" placeholder="Last name"  data-error="Please give your last name." required>
                                  <div class="help-block with-errors"></div>
                              </div>
                          </div>

                          <div class="form-group">
                              <label for="email">Email address<sup>*</sup></label>
                              <input type="email" class="form-control" id="email" name="email" placeholder="Email address" data-error="Please give your email address." required>
                              <div class="help-block with-errors"></div>
                          </div>
                      </div>

                        <div class="formtype__section formtype__section--details">
                            <h4>Company Information</h4>
                            <div class="form-group">
                                <label for="comp_name">Company Name<sup>*</sup></label>
                                <input type="text" class="form-control" id="comp_name" name="comp_name" placeholder="Company name" data-error="Please give a company name." required>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label for="comp_activity">Company Activity<sup>*</sup></label>
                                <div class=formtype__select>
                                    <select id="comp_activity" name="comp_activity" class="form-control" data-error="Please give a company activity." required>
                                        <option value="0">Please choose...</option>
                                        <option value="agency">Agency</option>
                                        <option value="production">Production</option>
                                        <option value="advertiser client">Advertiser / Client</option>
                                        <option value="media owner">Media Owner</option>
                                        <option value="npo">Government / Not for Profit / Education</option>
                                    </select>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label for="comp_type">Company Type<sup>*</sup></label>

                                <div class=formtype__select>
                                    <select id="comp_type" name="comp_type" class="form-control" data-error="Please give a company type." required>
                                        <option value="0">Please choose...</option>
                                    </select>
                                </div>
                                <div class="help-block with-errors"></div>

                                <!-- agency -->
                                <select class="hidden agency__select">
                                    <option value="0" selected="selected">Please choose...</option>
                                    <option value="brand activation">Brand Activation</option>
                                    <option value="brand agency">Brand Agency</option>
                                    <option value="brand content">Branded Content</option>
                                    <option value="consultancy/professional services">Consultancy / Professional Services</option>
                                    <option value="creative agency">Creative Agency</option>
                                    <option value="design agency">Design Agency/Studio</option>
                                    <option value="digital agency">Digital Agency</option>
                                    <option value="direct marketing agency">Direct Marketing Agency</option>
                                    <option value="events specialist">Events Specialist</option>
                                    <option value="full service agency">Full Service Agency</option>
                                    <option value="freelance/individual">Freelance/Individual</option>
                                    <option value="media agency">Media Agency</option>
                                    <option value="mobile agency">Mobile Agency</option>
                                    <option value="pr agency">PR Agency</option>
                                    <option value="product design">Product Design</option>
                                    <option value="research, data and analytics">Research, Data &amp; Analytics</option>
                                    <option value="search and social agency">Search &amp; Social Agency</option>
                                    <option value="specialist healthcare agency">Specialist Healthcare Agency</option>
                                </select>

                                <!-- production -->
                                <select class="hidden production__select">
                                    <option value="0" selected="selected">Please choose...</option>
                                    <option value="animation/illustration">Animation, Illustration, CGI &amp; 3D Services</option>
                                    <option value="design agency">Design Agency/Studio</option>
                                    <option value="digital production">Digital Production</option>
                                    <option value="editing">Editing</option>
                                    <option value="film production">Film Production</option>
                                    <option value="freelance/individual">Freelance/Individual</option>
                                    <option value="music/sound production">Music/Sound Production</option>
                                    <option value="photography">Photography</option>
                                    <option value="product design">Product Design</option>
                                    <option value="radio production">Radio Production</option>
                                </select>

                                <!-- adviser/client -->
                                <select class="hidden adviser-client__select">
                                    <option value="0" selected="selected">Please choose...</option>
                                    <option value="associations">Associations</option>
                                    <option value="automative and transport">Automotive &amp; Transport</option>
                                    <option value="business to business">Business to Business</option>
                                    <option value="consultancy/professional services">Consultancy/Professional Services</option>
                                    <option value="consumer electronics">Consumer Electronics</option>
                                    <option value="entertainment">Entertainment</option>
                                    <option value="environmental/utility company">Environmental/Utility Company</option>
                                    <option value="events">Events</option>
                                    <option value="financial">Financial</option>
                                    <option value="food and drink">Food &amp; Drink - Fast Moving Consumer Goods</option>
                                    <option value="freelance/individual">Freelance/Individual</option>
                                    <option value="government sector">Government Sector</option>
                                    <option value="health and beauty">Health &amp; Beauty - Fast Moving Consumer Goods</option>
                                    <option value="healthcare">Healthcare</option>
                                    <option value="IT services and software">IT Services &amp; Software</option>
                                    <option value="other">Other</option>
                                    <option value="other packaged goods">Other Packaged Goods</option>
                                    <option value="research and data">Research, Data &amp; Analytics</option>
                                    <option value="retail">Retail</option>
                                    <option value="search and social">Search &amp; Social</option>
                                    <option value="telecomms">Telecomms</option>
                                </select>

                                <!-- media owner -->
                                <select class="hidden media__select">
                                    <option value="0" selected="selected">Please choose...</option>
                                    <option value="entertainment">Entertainment</option>
                                    <option value="freelance/individual">Freelance/Individual</option>
                                    <option value="out of home or online media owner">Out of Home or Online Media Owner</option>
                                    <option value="search and social">Search &amp; Social</option>
                                </select>

                                <!-- npo -->
                                <select class="hidden npo__select">
                                    <option value="0" selected="selected">Please choose...</option>
                                    <option value="associations">Associations</option>
                                    <option value="charity/ngo">Charity/NGO</option>
                                    <option value="education">Education</option>
                                    <option value="freelance/individual">Freelance/Individual</option>
                                    <option value="government/regulator">Government/Regulator</option>
                                </select>
                            </div>
                            <div class="form-group hidden">
                                <label for="agency_network">Agency Network<sup>*</sup><small>Either wholly or majority-owned</small></label>
                                <div class=formtype__select>
                                    <select id="agency_network" name="agency_network" class="form-control" data-error="Please choose an agency network." required>
                					    <option value="0">Please choose an agency network...</option>

                						<option value="ASATSU-DK">ASATSU-DK</option>

                						<option value="BARTLE BOGLE HEGARTY">BARTLE BOGLE HEGARTY</option>

                						<option value="BATES CHI &amp; PARTNERS">BATES CHI &amp; PARTNERS</option>

                						<option value="BBDO WORLDWIDE">BBDO WORLDWIDE</option>

                						<option value="BPN">BPN</option>

                						<option value="CARAT">CARAT</option>

                						<option value="CDM GROUP">CDM GROUP</option>

                						<option value="CHEIL WORLDWIDE">CHEIL WORLDWIDE</option>

                						<option value="CMG">CMG</option>

                						<option value="COSSETTE COMMUNICATIONS GROUP">COSSETTE COMMUNICATIONS GROUP</option>

                						<option value="DAS">DAS</option>

                						<option value="DDB HEALTHCARE GROUP">DDB HEALTHCARE GROUP</option>

                						<option value="DDB WORLDWIDE">DDB WORLDWIDE</option>

                						<option value="DENTSU">DENTSU</option>

                						<option value="DENTSU MEDIA">DENTSU MEDIA</option>

                						<option value="DIGITAS LBI">DIGITAS LBI</option>

                						<option value="DMB&amp;B">DMB&amp;B</option>

                						<option value="FALLON WORLDWIDE">FALLON WORLDWIDE</option>

                						<option value="FCB">FCB</option>

                						<option value="FCB HEALTH">FCB HEALTH</option>

                						<option value="GEOMETRY GLOBAL">GEOMETRY GLOBAL</option>

                						<option value="GREY">GREY</option>

                						<option value="GREY HEALTHCARE GROUP">GREY HEALTHCARE GROUP</option>

                						<option value="HAKUHODO">HAKUHODO</option>

                						<option value="HAVAS HEALTH">HAVAS HEALTH</option>

                						<option value="HAVAS MEDIA GROUP">HAVAS MEDIA GROUP</option>

                						<option value="HAVAS WORLDWIDE">HAVAS WORLDWIDE</option>

                						<option value="HEARTS &amp; SCIENCE">HEARTS &amp; SCIENCE</option>

                						<option value="HILL &amp; KNOWLTON STRATEGIES">HILL &amp; KNOWLTON STRATEGIES</option>

                						<option value="INDIGENUS">INDIGENUS</option>

                						<option value="INITIATIVE">INITIATIVE</option>

                						<option value="INVENTIV">INVENTIV</option>

                						<option value="J. WALTER THOMPSON">J. WALTER THOMPSON</option>

                						<option value="KINETIC">KINETIC</option>

                						<option value="LEO BURNETT">LEO BURNETT</option>

                						<option value="M&amp;C SAATCHI">M&amp;C SAATCHI</option>

                						<option value="MAXUS">MAXUS</option>

                						<option value="MCCANN HEALTH">MCCANN HEALTH</option>

                						<option value="MCCANN WORLDGROUP">MCCANN WORLDGROUP</option>

                						<option value="MCGARRYBOWEN">MCGARRYBOWEN</option>

                						<option value="MDC PARTNERS">MDC PARTNERS</option>

                						<option value="MEC">MEC</option>

                						<option value="MEDIACOM">MEDIACOM</option>

                						<option value="MEDIAVEST|SPARK">MEDIAVEST|SPARK</option>

                						<option value="MINDSHARE">MINDSHARE</option>

                						<option value="0MOMENTUM">MOMENTUM</option>

                						<option value="MSL GROUP">MSL GROUP</option>

                						<option value="MULLENLOWE GROUP">MULLENLOWE GROUP</option>

                						<option value="0027">OGILVY &amp; MATHER</option>

                						<option value="OGILVY COMMONHEALTH">OGILVY COMMONHEALTH</option>

                						<option value="OMD WORLDWIDE">OMD WORLDWIDE</option>

                						<option value="OPTIMEDIA|BLUE">OPTIMEDIA|BLUE</option>

                						<option value="PERFORMICS">PERFORMICS</option>

                						<option value="PHD WORLDWIDE">PHD WORLDWIDE</option>

                						<option value="POSSIBLE WORLDWIDE">POSSIBLE WORLDWIDE</option>

                						<option value="POSTERSCOPE">POSTERSCOPE</option>

                						<option value="PUBLICIS HEALTHCARE COMMUNICATIONS GROUP">PUBLICIS HEALTHCARE COMMUNICATIONS GROUP</option>

                						<option value="PUBLICIS WORLDWIDE">PUBLICIS WORLDWIDE</option>

                						<option value="RAZORFISH">RAZORFISH</option>

                						<option value="SAATCHI &amp; SAATCHI">SAATCHI &amp; SAATCHI</option>

                						<option value="SAPIENTNITRO">SAPIENTNITRO</option>

                						<option value="SAPIENTRAZORFISH">SAPIENTRAZORFISH</option>

                						<option value="SCHOLZ &amp; FRIENDS">SCHOLZ &amp; FRIENDS</option>

                						<option value="SERVICEPLAN AGENTURGRUPPE">SERVICEPLAN AGENTURGRUPPE</option>

                						<option value="STARCOM">STARCOM</option>

                						<option value="STW GROUP">STW GROUP</option>

                						<option value="SUDLER &amp; HENNESSEY">SUDLER &amp; HENNESSEY</option>

                						<option value="TBWA WORLDWIDE">TBWA WORLDWIDE</option>

                						<option value="TBWA\WORLDHEALTH">TBWA\WORLDHEALTH</option>

                						<option value="THE BRAND UNION">THE BRAND UNION</option>

                						<option value="THE NORTH ALLIANCE">THE NORTH ALLIANCE</option>

                						<option value="THE UNITED NETWORK">THE UNITED NETWORK</option>

                						<option value="UM">UM</option>

                						<option value="VCCP HEALTH">VCCP HEALTH</option>

                						<option value="VIZEUM">VIZEUM</option>

                						<option value="WE ARE SOCIAL">WE ARE SOCIAL</option>

                						<option value="WIEDEN &amp; KENNEDY">WIEDEN &amp; KENNEDY</option>

                						<option value="YOUNG &amp; RUBICAM GROUP">YOUNG &amp; RUBICAM GROUP</option>

                						<option value="ZENITH">ZENITH</option>

                						<option value="ZENITHOPTIMEDIA">ZENITHOPTIMEDIA</option>

                						<option value="MIXED OWNERSHIP">MIXED OWNERSHIP</option>

                						<option value="ALL OTHER COMPANIES">ALL OTHER COMPANIES</option>

                                    </select>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>

                            <div class="form-group hidden">
                                <label for="holding_company">Holding Company<sup>*</sup></label>

                                <div class=formtype__select>
                                    <select id="holding_company" name="holding_company" class="form-control" data-error="Please give a holding company." required>
                                        <option value="0">Please choose...</option>

                						<option value="ALL OTHER COMPANIES">ALL OTHER COMPANIES</option>

                						<option value="BLUEFOCUS COMMUNICATIONS GROUP">BLUEFOCUS COMMUNICATIONS GROUP</option>

                						<option value="DENTSU GROUP">DENTSU GROUP</option>

                						<option value="ENERO">ENERO</option>

                						<option value="HAKUHODO DY HOLDINGS">HAKUHODO DY HOLDINGS</option>

                						<option value="HAVAS GROUP">HAVAS GROUP</option>

                						<option value="INTERPUBLIC GROUP">INTERPUBLIC GROUP</option>

                						<option value="MDC PARTNERS">MDC PARTNERS</option>

                						<option value="OMNICOM">OMNICOM</option>

                						<option value="PUBLICIS GROUPE">PUBLICIS GROUPE</option>

                						<option value="WPP">WPP</option>

                						<option value="MIXED OWNERSHIP">MIXED OWNERSHIP</option>

                                    </select>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>


                            <div class="form-group">
                                <label for="address">Address<sup>*</sup></label>
                                <input type="text" class="form-control" name="address" id="address" placeholder="Address" data-error="Please give an address." required>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="formtype__double">
                                <div class="form-group">
                                    <label for="po_box">PO Box</label>
                                    <input type="text" class="form-control" name="po_box" id="po_box" placeholder="PO Box">
                                </div>
                                <div class="form-group">
                                    <label for="city">City<sup>*</sup></label>
                                    <input type="text" class="form-control" name="city" id="city" placeholder="City" data-error="Please give a city." required>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="formtype__double">
                                <div class="form-group">
                                    <label for="postcode">Postcode/Zip<sup>*</sup></label>
                                    <input type="text" class="form-control" name="postcode" id="postcode" data-error="Please give a postcode/zip." placeholder="Postcode/Zip" required>
                                    <div class="help-block with-errors"></div>
                                </div>
                                <div class="form-group">
                                    <label for="country">Country<sup>*</sup></label>
                                    <div class=formtype__select>
                                        <select id="country" name="country" class="form-control" data-error="Please choose a country." required>
                                            <option value="0">Please choose a country....</option>

                                            <option value="AFGHANISTAN">AFGHANISTAN</option>

                                            <option value="ALBANIA">ALBANIA</option>

                                            <option value="ALGERIA">ALGERIA</option>

                                            <option value="ANDORRA">ANDORRA</option>

                                            <option value="ANGOLA">ANGOLA</option>

                                            <option value="ANTIGUA AND BARBUDA">ANTIGUA AND BARBUDA</option>

                                            <option value="ARGENTINA">ARGENTINA</option>

                                            <option value="ARMENIA">ARMENIA</option>

                                            <option value="AUSTRALIA">AUSTRALIA</option>

                                            <option value="AUSTRIA">AUSTRIA</option>

                                            <option value="AZERBAIJAN">AZERBAIJAN</option>

                                            <option value="BAHAMAS">BAHAMAS</option>

                                            <option value="BAHRAIN">BAHRAIN</option>

                                            <option value="BANGLADESH">BANGLADESH</option>

                                            <option value="BARBADOS">BARBADOS</option>

                                            <option value="BELARUS">BELARUS</option>

                                            <option value="BELGIUM">BELGIUM</option>

                                            <option value="BELIZE">BELIZE</option>

                                            <option value="BENIN">BENIN</option>

                                            <option value="BERMUDA">BERMUDA</option>

                                            <option value="BHUTAN">BHUTAN</option>

                                            <option value="BOLIVIA">BOLIVIA</option>

                                            <option value="BOSNIA &amp; HERZEGOVINA">BOSNIA &amp; HERZEGOVINA</option>

                                            <option value="BOTSWANA">BOTSWANA</option>

                                            <option value="BRAZIL">BRAZIL</option>

                                            <option value="BRITISH VIRGIN ISLES">BRITISH VIRGIN ISLES</option>

                                            <option value="BRUNEI DARUSSALAM">BRUNEI DARUSSALAM</option>

                                            <option value="BULGARIA">BULGARIA</option>

                                            <option value="BURKINA FASO">BURKINA FASO</option>

                                            <option value="BURUNDI">BURUNDI</option>

                                            <option value="CAMBODIA">CAMBODIA</option>

                                            <option value="CAMEROON">CAMEROON</option>

                                            <option value="CANADA">CANADA</option>

                                            <option value="CAPE VERDE">CAPE VERDE</option>

                                            <option value="CENTRAL AFRICAN REPUBLIC">CENTRAL AFRICAN REPUBLIC</option>

                                            <option value="CHAD">CHAD</option>

                                            <option value="CHILE">CHILE</option>

                                            <option value="CHINA">CHINA</option>

                                            <option value="CHINESE TAIPEI">CHINESE TAIPEI</option>

                                            <option value="COLOMBIA">COLOMBIA</option>

                                            <option value="COMOROS">COMOROS</option>

                                            <option value="COSTA RICA">COSTA RICA</option>

                                            <option value="CÔTE D'IVOIRE">CÔTE D'IVOIRE</option>

                                            <option value="CROATIA">CROATIA</option>

                                            <option value="CUBA">CUBA</option>

                                            <option value="CYPRUS">CYPRUS</option>

                                            <option value="CZECH REPUBLIC">CZECH REPUBLIC</option>

                                            <option value="DENMARK">DENMARK</option>

                                            <option value="DJIBOUTI">DJIBOUTI</option>

                                            <option value="175">DOMINICAN REPUBLIC</option>

                                            <option value="ECUADOR">ECUADOR</option>

                                            <option value="EGYPT">EGYPT</option>

                                            <option value="EL SALVADOR">EL SALVADOR</option>

                                            <option value="EQUATORIAL GUINEA">EQUATORIAL GUINEA</option>

                                            <option value="ERITREA">ERITREA</option>

                                            <option value="ESTONIA">ESTONIA</option>

                                            <option value="ETHIOPIA">ETHIOPIA</option>

                                            <option value="FIJI">FIJI</option>

                                            <option value="FINLAND">FINLAND</option>

                                            <option value="FRANCE">FRANCE</option>

                                            <option value="FRENCH WEST INDIES">FRENCH WEST INDIES</option>

                                            <option value="GABON">GABON</option>

                                            <option value="GAMBIA, THE">GAMBIA, THE</option>

                                            <option value="GEORGIA">GEORGIA</option>

                                            <option value="GERMANY">GERMANY</option>

                                            <option value="GHANA">GHANA</option>

                                            <option value="GIBRALTAR">GIBRALTAR</option>

                                            <option value="GLOBAL">GLOBAL</option>

                                            <option value="GREECE">GREECE</option>

                                            <option value="GRENADA">GRENADA</option>

                                            <option value="GUATEMALA">GUATEMALA</option>

                                            <option value="GUINEA">GUINEA</option>

                                            <option value="GUINEA-BISSAU">GUINEA-BISSAU</option>

                                            <option value="GUYANA">GUYANA</option>

                                            <option value="HAITI">HAITI</option>

                                            <option value="HONDURAS">HONDURAS</option>

                                            <option value="HONG KONG">HONG KONG</option>

                                            <option value="HUNGARY">HUNGARY</option>

                                            <option value="ICELAND">ICELAND</option>

                                            <option value="INDIA">INDIA</option>

                                            <option value="INDONESIA">INDONESIA</option>

                                            <option value="IRAN">IRAN</option>

                                            <option value="IRAQ">IRAQ</option>

                                            <option value="IRELAND">IRELAND</option>

                                            <option value="ISRAEL">ISRAEL</option>

                                            <option value="ITALY">ITALY</option>

                                            <option value="IVORY COAST">IVORY COAST</option>

                                            <option value="JAMAICA">JAMAICA</option>

                                            <option value="JAPAN">JAPAN</option>

                                            <option value="JORDAN">JORDAN</option>

                                            <option value="KAZAKHSTAN">KAZAKHSTAN</option>

                                            <option value="KENYA">KENYA</option>

                                            <option value="KIRIBATI">KIRIBATI</option>

                                            <option value="KOSOVO">KOSOVO</option>

                                            <option value="KUWAIT">KUWAIT</option>

                                            <option value="KYRGYZSTAN">KYRGYZSTAN</option>

                                            <option value="LAOS">LAOS</option>

                                            <option value="LATVIA">LATVIA</option>

                                            <option value="LEBANON">LEBANON</option>

                                            <option value="LESOTHO">LESOTHO</option>

                                            <option value="LIBERIA">LIBERIA</option>

                                            <option value="LIBYA">LIBYA</option>

                                            <option value="LIECHTENSTEIN">LIECHTENSTEIN</option>

                                            <option value="LITHUANIA">LITHUANIA</option>

                                            <option value="LUXEMBOURG">LUXEMBOURG</option>

                                            <option value="MACEDONIA">MACEDONIA</option>

                                            <option value="MADAGASCAR">MADAGASCAR</option>

                                            <option value="MALAWI">MALAWI</option>

                                            <option value="MALAYSIA">MALAYSIA</option>

                                            <option value="MALDIVES">MALDIVES</option>

                                            <option value="MALI">MALI</option>

                                            <option value="MALTA">MALTA</option>

                                            <option value="MARSHALL ISLANDS">MARSHALL ISLANDS</option>

                                            <option value="MAURITANIA">MAURITANIA</option>

                                            <option value="MAURITIUS">MAURITIUS</option>

                                            <option value="MEXICO">MEXICO</option>

                                            <option value="MICRONESIA">MICRONESIA</option>

                                            <option value="MOLDOVA">MOLDOVA</option>

                                            <option value="180">MONACO</option>

                                            <option value="MONGOLIA">MONGOLIA</option>

                                            <option value="MONTENEGRO">MONTENEGRO</option>

                                            <option value="MOROCCO">MOROCCO</option>

                                            <option value="MOZAMBIQUE">MOZAMBIQUE</option>

                                            <option value="MYANMAR">MYANMAR</option>

                                            <option value="NAMIBIA">NAMIBIA</option>

                                            <option value="NAURU">NAURU</option>

                                            <option value="NEPAL">NEPAL</option>

                                            <option value="NEW ZEALAND">NEW ZEALAND</option>

                                            <option value="NICARAGUA">NICARAGUA</option>

                                            <option value="NIGER">NIGER</option>

                                            <option value="NIGERIA">NIGERIA</option>

                                            <option value="NORWAY">NORWAY</option>

                                            <option value="OMAN">OMAN</option>

                                            <option value="PAKISTAN">PAKISTAN</option>

                                            <option value="PALAU">PALAU</option>

                                            <option value="PALESTINIAN STATE">PALESTINIAN STATE</option>

                                            <option value="PANAMA">PANAMA</option>

                                            <option value="PAPUA NEW GUINEA">PAPUA NEW GUINEA</option>

                                            <option value="PARAGUAY">PARAGUAY</option>

                                            <option value="PERU">PERU</option>

                                            <option value="POLAND">POLAND</option>

                                            <option value="PORTUGAL">PORTUGAL</option>

                                            <option value="PUERTO RICO">PUERTO RICO</option>

                                            <option value="QATAR">QATAR</option>

                                            <option value="REPUBLIQUE DEMOCRATIQUE DU CONGO">REPUBLIQUE DEMOCRATIQUE DU CONGO</option>

                                            <option value="ROMANIA">ROMANIA</option>

                                            <option value="RUSSIA">RUSSIA</option>

                                            <option value="RWANDA">RWANDA</option>

                                            <option value="SAMOA">SAMOA</option>

                                            <option value="SAN MARINO">SAN MARINO</option>

                                            <option value="SÃO TOMÉ AND PRÍNCIPE">SÃO TOMÉ AND PRÍNCIPE</option>

                                            <option value="SAUDI ARABIA">SAUDI ARABIA</option>

                                            <option value="SENEGAL">SENEGAL</option>

                                            <option value="SERBIA">SERBIA</option>

                                            <option value="SEYCHELLES">SEYCHELLES</option>

                                            <option value="SIERRA LEONE">SIERRA LEONE</option>

                                            <option value="SINGAPORE">SINGAPORE</option>

                                            <option value="SLOVAK REPUBLIC">SLOVAK REPUBLIC</option>

                                            <option value="SLOVENIA">SLOVENIA</option>

                                            <option value="SOLOMON ISLANDS">SOLOMON ISLANDS</option>

                                            <option value="SOMALIA">SOMALIA</option>

                                            <option value="SOUTH AFRICA">SOUTH AFRICA</option>

                                            <option value="SOUTH KOREA">SOUTH KOREA</option>

                                            <option value="SPAIN">SPAIN</option>

                                            <option value="SRI LANKA">SRI LANKA</option>

                                            <option value="ST. KITTS AND NEVIS">ST. KITTS AND NEVIS</option>

                                            <option value="ST. LUCIA">ST. LUCIA</option>

                                            <option value="ST. VINCENT AND THE GRENADINES">ST. VINCENT AND THE GRENADINES</option>

                                            <option value="SUDAN">SUDAN</option>

                                            <option value="SURINAME">SURINAME</option>

                                            <option value="SWAZILAND">SWAZILAND</option>

                                            <option value="SWEDEN">SWEDEN</option>

                                            <option value="SWITZERLAND">SWITZERLAND</option>

                                            <option value="SYRIA">SYRIA</option>

                                            <option value="TAJIKISTAN">TAJIKISTAN</option>

                                            <option value="TANZANIA">TANZANIA</option>

                                            <option value="THAILAND">THAILAND</option>

                                            <option value="THE NETHERLANDS">THE NETHERLANDS</option>

                                            <option value="THE NETHERLANDS ANTILLES">THE NETHERLANDS ANTILLES</option>

                                            <option value="THE PHILIPPINES">THE PHILIPPINES</option>

                                            <option value="TOGO">TOGO</option>

                                            <option value="TONGA">TONGA</option>

                                            <option value="TRINIDAD &amp; TOBAGO">TRINIDAD &amp; TOBAGO</option>

                                            <option value="TUNISIA">TUNISIA</option>

                                            <option value="TURKEY">TURKEY</option>

                                            <option value="TURKMENISTAN">TURKMENISTAN</option>

                                            <option value="TUVALU">TUVALU</option>

                                            <option value="UGANDA">UGANDA</option>

                                            <option value="UKRAINE">UKRAINE</option>

                                            <option value="UNION OF MYANMAR">UNION OF MYANMAR</option>

                                            <option value="UNITED ARAB EMIRATES">UNITED ARAB EMIRATES</option>

                                            <option value="UNITED KINGDOM">UNITED KINGDOM</option>

                                            <option value="URUGUAY">URUGUAY</option>

                                            <option value="USA">USA</option>

                                            <option value="UZBEKISTAN">UZBEKISTAN</option>

                                            <option value="VANUATU">VANUATU</option>

                                            <option value="VATICAN CITY (HOLY SEE)">VATICAN CITY (HOLY SEE)</option>

                                            <option value="VENEZUELA">VENEZUELA</option>

                                            <option value="VIETNAM">VIETNAM</option>

                                            <option value="WESTERN SAHARA">WESTERN SAHARA</option>

                                            <option value="YEMEN">YEMEN</option>

                                            <option value="ZAMBIA">ZAMBIA</option>

                                            <option value="ZIMBABWE">ZIMBABWE</option>
                                        </select>
                                    </div>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="main_switchboard">Main Switchboard<sup>*</sup></label>
                                <input type="text" class="form-control" name="main_switchboard" id="main_switchboard" placeholder="Main Switchboard" required>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label for="direct_line">Direct Line<sup>*</sup></label>
                                <input type="text" class="form-control" name="direct_line" id="direct_line" placeholder="Direct Line" required>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label for="cell_phone">Cell Phone<sup>*</sup></label>
                                <input type="text" class="form-control" name="cell_phone" id="cell_phone" placeholder="Cell Phone" required>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label for="company_website">Company Website Address<sup>*</sup><small>Format www.mywebsite.com</small></label>
                                <input type="text" class="form-control" id="company_website" name="company_website" placeholder="Company Website Address" required>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>

                        <div class="formtype__section--prefs blue">
                            <div class="form-group">
                                <p>The information you provide on this site is processed by Turner Broadcasting System Europe Limited, at Turner House, 16 Great Marlborough Street, London W1F 7HS, United Kingdom (“Turner”). Turner will use your details to register on your behalf on the Cannes Lion website and to make flight and accommodation arrangements for you to attend the Cannes Lion festival (“Festival”). Turner will not use your data for any other purpose.</p>

                                <p>For the purposes set out above, any data you provide will be transferred to the company which organizes and manages the Festival (that is Ascential Events (Europe) Limited at The Prow, 1 Wilder Walk, London W1B 5AP, United Kingdom) and to the airline and hotel we will select for your travel and stay.</p>
                                <div class="formtype__radio">
                                    <label for="consent" class="radio-inline checkbox">
                                        <input name="legal-checkbox" type="checkbox" id="consent" required/>
                                        By clicking the box you consent to the data collection, use and transfer of your personal data as described above.
                                        <span class="checkbox"></span>
                                        <div class="help-block with-errors"></div>
                                    </label>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="col-md-4 col-sm-5 col-md-offset-1 col-sm-offset-0">
                        <div class="formtype__section">
                            <div class="formtype__section--prefs">
                                <h4>Flight Preferences</h4>
                                <div class="formtype__box">
                                    <div class="form-group">
                                        <label for="to_nice">To Nice<sup>*</sup></label>
                                        <div class=formtype__select>
                                            <select id="to_nice" name="to_nice_day" class="form-control" required>
                                                <option value="0">Choose a date...</option>
                                                <option value="Sunday 18th June">Sunday 18th June</option>
                                                <option value="Monday 19th June">Monday 19th June</option>
                                            </select>
                                        </div>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                    <div class="form-group">
                                        <div class="formtype__radio">
                                            <label for="am_1" class="radio-inline">
                                              <input type="radio" name="ampm_to" id="am_1" value="am_1" required> AM
                                              <span></span>
                                            </label>
                                        </div>

                                        <div class="formtype__radio">
                                            <label for="pm_1" class="radio-inline">
                                              <input type="radio" name="ampm_to" id="pm_1" value="pm_1" required> PM
                                              <span></span>
                                            </label>
                                        </div>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="from_nice">From Nice<sup>*</sup></label>
                                        <div class=formtype__select>
                                            <select id="from_nice" name="from_nice_day" class="form-control" required>
                                                <option value="0">Choose a date...</option>
                                                <option value="Wednesday 21st June">Wednesday 21st June</option>
                                                <option value="Thursday 22nd June">Thursday 22nd June</option>
                                            </select>
                                        </div>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                    <div class="form-group">
                                        <div class="formtype__radio">
                                        <label for="am_2" class="radio-inline">
                                          <input type="radio" name="ampm_from" id="am_2" value="am_2" required> AM
                                          <span></span>
                                        </label>
                                        </div>
                                        <div class="formtype__radio">
                                        <label for="pm_2" class="radio-inline">
                                          <input type="radio" name="ampm_from" id="pm_2" value="pm_2" required> PM
                                          <span></span>
                                        </label>
                                        </div>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>

                                <p>Please be advised that once this form has been submitted we will take it as confirmation and will proceed to purchase your 5-day delegate pass. We will also confirm your room booking and begin to arrange your flights details according to your requirements.</p>

                                <p class="formtype--small-print"><i><sup>**</sup>Your Delegate Pass is non-refundable to us and non-transferrable to others.</i></p>
                            </div>

                            <p>Once registered you will recieve an email prompting you to send us a photo to complete your registration. Please do so as soon as possible. Contact your CNN Sales Representative or the CNN Events Team if you have any questions.</p>


                            <!-- <div class="g-recaptcha"
                                  data-sitekey="6LfPIhoUAAAAAF21kYLHjUTf-m-zI0j2w-HyUcBl"
                                  data-callback="onSubmit"
                                  data-size="invisible">
                            </div> -->
                            <button id="SubmitRegistration" class="btn btn-ghost">
                            Submit registration
                            </button>
                        </div>
                    </div>
                </form>
        </div>
    </div>
</div>


<!--overlay-->
<div data-widget="confirmation" class="modal-conf">
    <a href="#" class="modal-conf--close">+</a>
    <div class="modal-conf__wrapper">
        <div class="thankyou">
            <h3>Thank you</h3>
            <p>You should recieve an email confirmation shortly. If you have any queries please contact <a href="mailTo:CNNEvents@cnn.com">CNNEvents@cnn.com</a>.</p>
        </div>
    </div>
<!-- </div> -->
<div class="modal-conf__bg"></div>

<?php include("./components/social-feed.php"); ?>

<?php include("./components/footer.php"); ?>

<?php include("./components/foot.php"); ?>
