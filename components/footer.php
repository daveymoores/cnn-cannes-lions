<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="footer__logos">
                    <a href="http://cnn.com"><img src="dist/images/cnn-logo.png" alt="" /></a>

                    <ul>
                        <li><a href="http://edition.cnn.com/travel"><img class="footer__logos--1" src="dist/images/footer-icons/cnn-logos.png" alt="CNN news logo" /></a></li>
                        <li><a href="http://www.greatbigstory.com/"><img class="footer__logos--2" src="dist/images/footer-icons/great-big-story.png" alt="Great big story logo" /></a></li>
                        <!-- <li><a href="http://edition.cnn.com/shows/anthony-bourdain-parts-unknown"><img class="footer__logos--3" src="dist/images/footer-icons/ab-pu.png" alt="Anthony Bourdain Logo" /></a></li> -->
                        <li><a href="http://money.cnn.com/technology/"><img class="footer__logos--4" src="dist/images/footer-icons/cnn-tech.png" alt="CNN Tech" /></a></li>
                    </ul>
                </div>

                <p class="footer__contact">For any other enquiries please contact the CNN Events Team on <a href="mailTo:CNNEvents@cnn.com">CNNEvents@cnn.com</a></p>

            </div>
        </div>
    </div>
</footer>
