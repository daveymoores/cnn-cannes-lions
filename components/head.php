<!DOCTYPE html>
<html lang="en">
	<head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/> <!--320-->
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>CNN Cannes Lions</title>

        <link href="/dist/styles/main.css?v1.0" rel="stylesheet">
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <script src="https://use.typekit.net/tqa7ttn.js"></script>
        <script>try{Typekit.load({ async: true });}catch(e){}</script>
        <script src='https://www.google.com/recaptcha/api.js'></script>
    </head>
    <body>
        <nav data-widget="nav-sidebar" class="pushy pushy-right">
            <div class="pushy-content">
                <ul>
					<li class="pushy-link"><a href="/index.php">Welcome</a></li>
                    <li class="pushy-link"><a href="/agenda.php">Agenda</a></li>
                    <li class="pushy-link"><a href="/need-to-know.php">Need To Know</a></li>
                    <li class="pushy-link"><a href="/gallery-2016.php">Gallery 2016</a></li>
                    <li class="pushy-link"><a href="/registration.php">Registration</a></li>
                </ul>
            </div>
        </nav>
        <!--pusher for site content-->
        <div id="container">
