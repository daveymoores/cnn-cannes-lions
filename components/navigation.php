<nav class="navbar cl-nav">
  <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="menu-btn navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="/index.php"><img src="dist/images/CNN_International_logo_2014.png" alt="" /></a>

      <div class="countdown" data-widget="countdown">
          <ul class="countdown__list">
              <li class="countdown__item">
                  <span>weeks</span>
                  <span class="countdown__item--weeks"></span>
              </li>
              <li class="countdown__item">
                  <span>days</span>
                  <span class="countdown__item--days"></span>
              </li>
              <li class="countdown__item">
                  <span>hours</span>
                  <span class="countdown__item--hours"></span>
              </li>
              <li class="countdown__item">
                  <span>min</span>
                  <span class="countdown__item--minutes"></span>
              </li>
              <!-- <li class="countdown__item">
                  <span>sec</span>
                  <span class="countdown__item--seconds"></span>
              </li> -->
          </ul>
      </div>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right">
        <li><a href="/index.php">Welcome</a></li>
        <li><a href="/agenda.php">Agenda</a></li>
        <li><a href="/need-to-know.php">Need To Know</a></li>
        <li><a href="/gallery-2016.php">Gallery 2016</a></li>
        <li><a href="/registration.php" class="btn btn-yellow">Registration</a></li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
