<section class="twitter-feed" data-widget="init-tweet">
    <div class="twitter-feed__panel"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <img src="dist/images/transparent-cnn-logo.png" class="footer--watermark" alt="cnn logo" />
                <h2>Follow CNN on Social Media</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="row twitter-feed__parent">
                    <div class="col-md-4 col-sm-6">
                        <div class="twitter-feed__item">

                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6">
                        <div class="twitter-feed__item">

                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6">
                        <div class="twitter-feed__item">

                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6">
                        <div class="twitter-feed__item">

                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6">
                        <div class="twitter-feed__item">

                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6">
                        <div class="twitter-feed__item">

                        </div>
                    </div>
                </div>
            </div>
            <!-- <div class="col-md-4">
                <div class="instagram-feed__wrapper">

                </div>
            </div> -->
        </div>
    </div>
</section>
