<?php include("./components/head.php"); ?>

<?php include("./components/navigation.php"); ?>

<div class="hero hero--welcome">
    <div class="container">
        <div class="row">
            <div class="col-md-7 col-sm-7 col-xs-6">
                <h2>Bienvenue!</h2>
                <a href="/registration.php" class="btn btn-yellow--lrg">Registration</a>
            </div>
            <div class="col-md-5 col-sm-5 col-xs-6">
                <img src="dist/images/lions-logo.png" alt="" />
            </div>
        </div>
    </div>
    <div class="hero__panel--blue"></div>
    <div class="hero__panel--body"></div>
</div>

<div class="site-content--wrapper site-content--wrapper--home">
    <div class="container">
        <div class="row">
            <div class="col-md-7">
                <div class="home-content">
                    <h3 class="home-content--heading">We would be delighted if you could join CNN at the Cannes Lions 63rd Festival...</h3>
                    <p class="home-content--copy">for the industry’s marquee event on creativity, inspiration and much more. At Cannes, CNN will also be hosting a series of exclusive VIP events and will welcome you to the CNN Cabana in Beach Town. Just a stone’s throw away from the Palais des Festivals on the glistening Mediterranean, the cabana will be a great place to replenish throughout the day and network in the sunshine. Accommodation, travel and delegate passes will be provided.</p>
                </div>
            </div>
            <div class="col-md-5">
                <a href="/agenda.php" class="btn btn-ghost home-content--btn">See The Agenda</a>
            </div>
        </div>
    </div>
</div>

<?php include("./components/social-feed.php"); ?>

<?php include("./components/footer.php"); ?>

<?php include("./components/foot.php"); ?>
