<?php include("./components/head.php"); ?>

<?php include("./components/navigation.php"); ?>

<div class="hero hero--agenda">
    <div class="container">
        <div class="row">
            <div class="col-md-7 col-sm-7 col-xs-6">
                <h2>Agenda</h2>
            </div>
            <div class="col-md-5 col-sm-5 col-xs-6">
                <img src="dist/images/lions-logo.png" alt="" />
            </div>
        </div>
    </div>
    <div class="hero__panel--blue"></div>
    <div class="hero__panel--body"></div>
</div>


<div class="site-content--wrapper">
    <div class="container">
        <div class="row">
            <div class="col-md-12">

                <div class="day">
                    <div class="day__date">
                        <h4>
                            <span class="day__date--day-name">Monday</span><span class="day__date--number">19<sup>th</sup> <span>June</span></span>
                        </h4>
                    </div>
                    <div class="day__events">
                        <div class="day__events--item">
                            <span>All Day</span>
                            <span>Guest Arrivals</span>
                        </div>
                        <div class="day__events--item">
                            <span>10:00</span>
                            <span>Cannes Lions Festival Opens</span>
                        </div>
                        <div class="day__events--item">
                            <span>21:30</span>
                            <span>Official Cannes Lions Opening Gala</span>
                        </div>
                    </div>
                </div>

                <div class="day">
                    <div class="day__date">
                        <h4>
                            <span class="day__date--day-name">Tuesday</span><span class="day__date--number">20<sup>th</sup> <span>June</span></span>
                        </h4>
                    </div>
                    <div class="day__events">
                        <div class="day__events--item">
                            <span>10:00</span>
                            <span>Cannes Lions Festival Continues</span>
                        </div>
                        <div class="day__events--item">
                            <span>12:30</span>
                            <span>Exclusive CNN Welcome Lunch at Carlton Beach Restaurant</span>
                        </div>
                        <div class="day__events--item">
                            <span>20:00</span>
                            <span>Exclusive CNN Dinner and After Party (transport from the Pullman at 19:00)</span>
                        </div>
                    </div>
                </div>

                <div class="day">
                    <div class="day__date">
                        <h4>
                            <span class="day__date--day-name">Wednesday</span><span class="day__date--number">21<sup>st</sup> <span>June</span></span>
                        </h4>
                    </div>
                    <div class="day__events">
                        <div class="day__events--item">
                            <span>10:00</span>
                            <span>Cannes Lions Festival Continues</span>
                        </div>
                        <div class="day__events--item">
                            <span>12:30</span>
                            <span>Time Warner Panel Session</span>
                        </div>
                        <div class="day__events--item">
                            <span>Followed By</span>
                            <span>CNN VIP Lunch &amp; Fireside Chat</span>
                        </div>
                    </div>
                </div>

                <div class="day">
                    <div class="day__date">
                        <h4>
                            <span class="day__date--day-name">Thursday</span><span class="day__date--number">22<sup>nd</sup> <span>June</span></span>
                        </h4>
                    </div>
                    <div class="day__events">
                        <div class="day__events--item">
                            <span>All Day</span>
                            <span>Guest Departures</span>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<?php include("./components/social-feed.php"); ?>

<?php include("./components/footer.php"); ?>

<?php include("./components/foot.php"); ?>
