function Currency(node){
    this.$node = $(node);

    this.init();
}

Currency.prototype.init = function(){
    var cxt = this;

    $.getJSON( "https://api.fixer.io/latest?base=USD", function( data ) {
      var items = [];
      var obj = $.parseJSON(JSON.stringify(data));

      $.each( obj, function( key, val ) {
        if(key == 'rates') {
            var eur = parseFloat(val.EUR).toFixed(2);
            items.push( "<span class='currency__currencies--europound'>$1 = €" + eur + "</span>" );
        }
      });

      cxt.$node.append(items);
     });


      $.getJSON( "https://api.fixer.io/latest?base=HKD", function( data ) {
        var items = [];
        var obj = $.parseJSON(JSON.stringify(data));
        //console.log(obj);
        $.each( obj, function( key, val ) {
          if(key == 'rates') {
              var eur = parseFloat(val.EUR).toFixed(2);
              items.push( "<span class='currency__currencies--hkdollar'>HK$1 = €" + eur + "</span>" );
          }
        });

        cxt.$node.append(items);
       });


        $.getJSON( "https://api.fixer.io/latest?base=GBP", function( data ) {
          var items = [];
          var obj = $.parseJSON(JSON.stringify(data));
          console.log(obj);
          $.each( obj, function( key, val ) {
            if(key == 'rates') {
                var eur = parseFloat(val.EUR).toFixed(2);
                items.push( "<span class='currency__currencies--europound'>£1 = €" + eur + "</span>" );
            }
          });

          cxt.$node.append(items);
         });
}

module.exports = Currency;
