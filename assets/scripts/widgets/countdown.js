function Countdown(node){
    this.$node = $(node);

    this.setVars();
    this.init();
}

Countdown.prototype.setVars = function(){
    this.css = {
        states : {
            init : 'initialised'
        },
        selectors : {
            weeks : '.countdown__item--weeks',
            days : '.countdown__item--days',
            hours : '.countdown__item--hours',
            minutes : '.countdown__item--minutes',
            seconds : '.countdown__item--seconds'
        }
    }
}

Countdown.prototype.init = function(){
    this.$weeks = this.$node.find(this.css.selectors.weeks);
    this.$days = this.$node.find(this.css.selectors.days);
    this.$hours = this.$node.find(this.css.selectors.hours);
    this.$minutes = this.$node.find(this.css.selectors.minutes);
    //this.$seconds = this.$node.find(this.css.selectors.seconds);

    var deadline = new Date(2017, 5, 17 , 9, 00, 00);
    var cxt = this;

    this.updateClock(deadline);
    this.timeinterval = setInterval(function(){return cxt.updateClock(deadline);}, 1000);
}

Countdown.prototype.updateClock = function(endtime){
    var cxt = this;
    var t = this.getTimeRemaining(endtime);

    this.$weeks.text(Math.floor(t.days/7));
    this.$days.text(t.days - (Math.floor(t.days/7)*7));
    this.$hours.text(('0' + t.hours).slice(-2));
    this.$minutes.text(('0' + t.minutes).slice(-2));
    //this.$seconds.text(('0' + t.seconds).slice(-2));

    if (t.total <= 0) {
      clearInterval(cxt.timeinterval);
    }
}

Countdown.prototype.getTimeRemaining = function(endtime){
    var t = Date.parse(endtime) - Date.parse(new Date());
    var seconds = Math.floor((t / 1000) % 60);
    var minutes = Math.floor((t / 1000 / 60) % 60);
    var hours = Math.floor((t / (1000 * 60 * 60)) % 24);
    var days = Math.floor(t / (1000 * 60 * 60 * 24));
    return {
      'total': t,
      'days': days,
      'hours': hours,
      'minutes': minutes,
      'seconds': seconds
    };
}

module.exports = Countdown;
