var validator = require('bootstrap-validator');

function FormBehaviour(node){
    this.$node = $(node);

    this.setVars();
    this.init();
}

FormBehaviour.prototype.setVars = function(){
    this.css = {
        states : {
            init : 'initialised'
        },
        selectors : {
            compActivity : '#comp_activity',
            compType : '#comp_type',
            holdingComp : '#holding_company',
            agencyNetwork : '#agency_network',
            selectContentsProduction : '.production__select',
            selectContentsAdviser : '.adviser-client__select',
            selectContentsMedia : '.media__select',
            selectContentsNpo : '.npo__select',
            selectContentsAgency : '.agency__select',
            formtype : '.formtype',
            submit : '#SubmitRegistration'
        }
    }
}

FormBehaviour.prototype.submitForm = function(e){
    this.$form.validator();
    if (e.isDefaultPrevented()) {
         console.log('form invalid');
    } else {
        if($('#stop-the-bots').val()=='') {
            this.$form.submit();
        } else {
            alert('Sorry there was a fault');
        }
    }
}

FormBehaviour.prototype.init = function(){
    this.$node.addClass(this.css.states.init);

    this.$compActivity = $(this.css.selectors.compActivity);
    this.$compType = $(this.css.selectors.compType);
    this.$holdingComp = $(this.css.selectors.holdingComp);
    this.$agencyNetwork = $(this.css.selectors.agencyNetwork);
    this.$compNetwork = $(this.css.selectors.compNetwork);
    this.$form = $(this.css.selectors.formtype);
    this.$submit = $(this.css.selectors.submit);

    this.$selectContentsProduction = this.$node.find(this.css.selectors.selectContentsProduction);
    this.$selectContentsAdviser = this.$node.find(this.css.selectors.selectContentsAdviser);
    this.$selectContentsMedia = this.$node.find(this.css.selectors.selectContentsMedia);
    this.$selectContentsNpo = this.$node.find(this.css.selectors.selectContentsNpo);
    this.$selectContentsAgency = this.$node.find(this.css.selectors.selectContentsAgency);

    this.$compActivity.change($.proxy(this.populate, this));
    this.$submit.on('click', $.proxy(this.submitForm, this));
}

FormBehaviour.prototype.populate = function(e){
    var value = $(e.currentTarget).val(),
        cxt = this,
        selectContents = '';

    function toggleAgencyFields(bool){
        if(bool) {
            cxt.$holdingComp.parent().parent().removeClass('hidden');
            cxt.$agencyNetwork.parent().parent().removeClass('hidden');
        } else {
            cxt.$holdingComp.parent().parent().addClass('hidden');
            cxt.$agencyNetwork.parent().parent().addClass('hidden');
        }
    }

    switch (value) {
        case 'agency':
            selectContents = this.$selectContentsAgency.contents().clone();
            toggleAgencyFields(true);
            break;
        case 'production':
            selectContents = this.$selectContentsProduction.contents().clone();
            toggleAgencyFields(false);
            break;
        case 'advertiser client':
            selectContents = this.$selectContentsAdviser.contents().clone();
            toggleAgencyFields(false);
            break;
        case 'media owner':
            selectContents = this.$selectContentsMedia.contents().clone();
            toggleAgencyFields(false);
            break;
        case 'npo':
            selectContents = this.$selectContentsNpo.contents().clone();
            toggleAgencyFields(false);
            break;
    }

    this.$compType.empty();
    this.$compType.append(selectContents);
}

module.exports = FormBehaviour;
