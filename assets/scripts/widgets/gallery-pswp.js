var PhotoSwipe = require('photoswipe');
var PhotoSwipeUI_Default = require('photoswipe/dist/photoswipe-ui-default.js');

function GalleryPswp(){

    this.pswpElement = document.querySelectorAll('.pswp')[0];
    this.items = [
        {
            src: 'https://farm2.staticflickr.com/1043/5186867718_06b2e9e551_b.jpg',
            w: 964,
            h: 1024
        },
        {
            src: 'https://farm7.staticflickr.com/6175/6176698785_7dee72237e_b.jpg',
            w: 1024,
            h: 683
        }
    ];

    this.options = {
        history: false,
        focus: false,

        showAnimationDuration: 0,
        hideAnimationDuration: 0
    };

    this.init();
}

GalleryPswp.prototype.init = function(){
    //this.openPhotoSwipe();
    //document.getElementById('btn').onclick = this.openPhotoSwipe();
}

GalleryPswp.prototype.openPhotoSwipe = function() {
    var cxt = this;
    var gallery = new PhotoSwipe( cxt.pswpElement, PhotoSwipeUI_Default, cxt.items, cxt.options);
    gallery.init();
}

module.exports = GalleryPswp;
