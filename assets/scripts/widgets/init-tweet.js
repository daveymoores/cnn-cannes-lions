var spectragram = require('spectragram');

function InitTweet(node){
    this.$node = $(node);

    this.setVars();
    this.init();
}

InitTweet.prototype.setVars = function(){
    this.css = {
        states : {
            init : 'initialised'
        },
        selectors : {
            twitterFeed : '.twitter-feed',
            twitterFeedItem : '.twitter-feed__item',
            twitterFeedItem : '.twitter-feed__item'
        }
    }
}

InitTweet.prototype.init = function(){
    this.$node.addClass(this.css.states.init);
    this.$twitterFeedItem = this.$node.find(this.css.selectors.twitterFeedItem);

    // removing twitter feed for now...
    // var Spectra = {
    //   instaToken: '3240273998.00dec1e.e3cb5cbe9b934570a66a8878475bb7f6',
    //   instaID: '00dec1e96bf249f298ffbdba97f9428a',
    //
    //   init: function () {
    //     $.fn.spectragram.accessData = {
    //       accessToken: this.instaToken,
    //       clientID: this.instaID
    //     };
    //
    //     $('.instagram-feed__wrapper').spectragram('getUserFeed',{
    //       max: 12,
    //       query: 'davey.moores',
    //       wrapEachWith: '<div class="instagram-feed__wrapper--item">'
    //     });
    //   }
    // }
    //
    // Spectra.init();


    var cxt = this;

    //webtask
    $.getJSON( "https://wt-cbaaabe73352022d767ac171a973e955-0.run.webtask.io/tweet", function( data ) {
      var items = [];
      var obj = $.parseJSON(JSON.stringify(data));

      $.each( obj.statuses, function( key, val ) {

        var string = val.created_at;
        string = string.split(" ");
        var stringArray = new Array();

        for(var i =0; i < 3; i++){
            stringArray.push(string[i]);
            if(i != string.length-1){
              stringArray.push(" ");
            }
        }

        var regex = /(https?:\/\/([-\w\.]+)+(:\d+)?(\/([\w\/_\.]*(\?\S+)?)?)?)/g;
        var replaced_text = val.text.replace(regex, "<a href='$1'>$1</a>")

        items.push( "<p class='date'><i class='fa fa-twitter' aria-hidden='true'></i>"+stringArray.join("")+"</p><a class='screenname' href='https://twitter.com/"+val.user.screen_name+"'>@"+val.user.screen_name+"</a><p data-id='" + val.id_str + "'>" + replaced_text + "</p>" );
      });

      cxt.$twitterFeedItem.each(function(i){
          $(this).append(items[i]);
      });
    });

}

module.exports = InitTweet;
