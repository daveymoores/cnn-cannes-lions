function confirmation(node){
    this.$node = $(node);

    this.setVars();
    this.init();
}

confirmation.prototype.init = function(){
    this.$modal = this.$node;
    this.$modalBg = this.$node.next();
    this.$modalClose = this.$node.find(this.css.selectors.modalClose);

    var url = window.location.href;
    var arr = url.split('?');

    if (url.length > 1 && arr[1] !== '' && arr[1] !== undefined) {
        this.$modal.addClass(this.css.states.active);
    }

    this.$modalClose.on('click', $.proxy(this.closeModal, this));
}

confirmation.prototype.setVars = function(){
    this.css = {
        states : {
            active : 'active'
        },
        selectors : {
            modal : '.modal-conf',
            modalBg : '.modal-conf__bg',
            modalClose : '.modal-conf--close'
        }
    }
}

confirmation.prototype.closeModal = function(){
    this.$modal.removeClass(this.css.states.active);
    this.$modalBg.removeClass(this.css.states.active);
}



module.exports = confirmation;
