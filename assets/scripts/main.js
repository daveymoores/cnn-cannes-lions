(function () {
	'use strict';

	window.jQuery = window.$ = require('jquery');

	var widgets;

	widgets = {
	   'init-tweet': require('./widgets/init-tweet.js'),
       'form-behaviour': require('./widgets/form-behaviour.js'),
	   'countdown': require('./widgets/countdown.js'),
	   'currency': require('./widgets/currency.js'),
	   'thumbnail-gallery': require('./widgets/thumbnail-gallery.js'),
	   'nav-sidebar': require('./widgets/nav-sidebar.js'),
	   'confirmation': require('./widgets/confirmation.js')
	};

	function initWidgets ($node) {
	   $node.find('[data-widget]').each(function () {
	       var type;
	       type = $(this).attr('data-widget');
	       if (widgets[type]) {
	           new widgets[type](this);
	       }
	   });
	}

	initWidgets($('body'));

	$('body').on('widgets', function (e) {
	   initWidgets($(e.target));
	});

}());
