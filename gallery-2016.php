<?php include("./components/head.php"); ?>

<?php include("./components/navigation.php"); ?>

<div class="hero hero--gallery">
    <div class="container">
        <div class="row">
            <div class="col-md-7 col-sm-7 col-xs-6">
                <h2>Gallery 2016</h2>
            </div>
            <div class="col-md-5 col-sm-5 col-xs-6">
                <img src="dist/images/lions-logo.png" alt="" />
            </div>
        </div>
    </div>
    <div class="hero__panel--blue"></div>
    <div class="hero__panel--body"></div>
</div>

<div class="site-content--wrapper">
    <div class="container">
        <div class="row">
            <div class="col-md-7">
                <div class='embed-container'><iframe src='https://player.vimeo.com/video/177367281' frameborder='0' webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe></div>
            </div>
            <div class="col-md-4 col-md-offset-1">
                <div class="video__text">
                    <h3>CNN Cannes Lions</h3>
                    <p>2016 was a great success for CNN at Cannes Lions Festival, our presence on the ground and events executed were the talk of the festival.  We hope to go above and beyond our achievements last year, but for now please have look through our photos to see what happened last year.</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">

                <h3 class="thumbnail-gallery__title">Gallery</h3>
                <div data-widget="thumbnail-gallery" class="thumbnail-gallery" itemscope itemtype="http://schema.org/ImageGallery">

                        <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                          <a href="./dist/images/gallery/1-xbTB4r5Y7GTHRd5O7vQSlQ.jpeg" itemprop="contentUrl" data-size="800x533">
                              <img src="./dist/images/gallery/thumbnails/1-xbTB4r5Y7GTHRd5O7vQSlQ.jpg" itemprop="thumbnail" alt="Image description" />
                          </a>
                        </figure>

                        <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                          <a href="./dist/images/gallery/GC4A3249.jpg" itemprop="contentUrl" data-size="800x533">
                              <img src="./dist/images/gallery/thumbnails/GC4A3249.jpg" itemprop="thumbnail" alt="Image description" />
                          </a>
                        </figure>

                        <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                          <a href="./dist/images/gallery/GC4A3253.jpg" itemprop="contentUrl" data-size="800x533">
                              <img src="./dist/images/gallery/thumbnails/GC4A3253.jpg" itemprop="thumbnail" alt="Image description" />
                          </a>
                        </figure>

                        <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                          <a href="./dist/images/gallery/GC4A3254.jpg" itemprop="contentUrl" data-size="800x533">
                              <img src="./dist/images/gallery/thumbnails/GC4A3254.jpg" itemprop="thumbnail" alt="Image description" />
                          </a>
                        </figure>

                        <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                          <a href="./dist/images/gallery/GC4A3259.jpg" itemprop="contentUrl" data-size="800x533">
                              <img src="./dist/images/gallery/thumbnails/GC4A3259.jpg" itemprop="thumbnail" alt="Image description" />
                          </a>
                        </figure>

                        <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                          <a href="./dist/images/gallery/GC4A3264.jpg" itemprop="contentUrl" data-size="800x533">
                              <img src="./dist/images/gallery/thumbnails/GC4A3264.jpg" itemprop="thumbnail" alt="Image description" />
                          </a>
                        </figure>

                        <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                          <a href="./dist/images/gallery/GC4A3274.jpg" itemprop="contentUrl" data-size="800x533">
                              <img src="./dist/images/gallery/thumbnails/GC4A3274.jpg" itemprop="thumbnail" alt="Image description" />
                          </a>
                        </figure>

                        <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                          <a href="./dist/images/gallery/GC4A3287.jpg" itemprop="contentUrl" data-size="800x533">
                              <img src="./dist/images/gallery/thumbnails/GC4A3287.jpg" itemprop="thumbnail" alt="Image description" />
                          </a>
                        </figure>

                        <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                          <a href="./dist/images/gallery/GC4A3301.jpg" itemprop="contentUrl" data-size="800x533">
                              <img src="./dist/images/gallery/thumbnails/GC4A3301.jpg" itemprop="thumbnail" alt="Image description" />
                          </a>
                        </figure>

                        <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                          <a href="./dist/images/gallery/GC4A3317.jpg" itemprop="contentUrl" data-size="800x533">
                              <img src="./dist/images/gallery/thumbnails/GC4A3317.jpg" itemprop="thumbnail" alt="Image description" />
                          </a>
                        </figure>

                        <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                          <a href="./dist/images/gallery/GC4A3318.jpg" itemprop="contentUrl" data-size="800x533">
                              <img src="./dist/images/gallery/thumbnails/GC4A3318.jpg" itemprop="thumbnail" alt="Image description" />
                          </a>
                        </figure>

                        <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                          <a href="./dist/images/gallery/GC4A3351.jpg" itemprop="contentUrl" data-size="800x533">
                              <img src="./dist/images/gallery/thumbnails/GC4A3351.jpg" itemprop="thumbnail" alt="Image description" />
                          </a>
                        </figure>

                        <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                          <a href="./dist/images/gallery/GC4A3361.jpg" itemprop="contentUrl" data-size="800x533">
                              <img src="./dist/images/gallery/thumbnails/GC4A3361.jpg" itemprop="thumbnail" alt="Image description" />
                          </a>
                        </figure>

                        <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                          <a href="./dist/images/gallery/GC4A3409.jpg" itemprop="contentUrl" data-size="800x533">
                              <img src="./dist/images/gallery/thumbnails/GC4A3409.jpg" itemprop="thumbnail" alt="Image description" />
                          </a>
                        </figure>

                        <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                          <a href="./dist/images/gallery/GC4A3444.jpg" itemprop="contentUrl" data-size="800x533">
                              <img src="./dist/images/gallery/thumbnails/GC4A3444.jpg" itemprop="thumbnail" alt="Image description" />
                          </a>
                        </figure>


                        <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                          <a href="./dist/images/gallery/GC4A3498.jpg" itemprop="contentUrl" data-size="800x533">
                              <img src="./dist/images/gallery/thumbnails/GC4A3498.jpg" itemprop="thumbnail" alt="Image description" />
                          </a>
                        </figure>

                        <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                          <a href="./dist/images/gallery/GC4A3560.jpg" itemprop="contentUrl" data-size="800x533">
                              <img src="./dist/images/gallery/thumbnails/GC4A3560.jpg" itemprop="thumbnail" alt="Image description" />
                          </a>
                        </figure>

                        <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                          <a href="./dist/images/gallery/GC4A3607.jpg" itemprop="contentUrl" data-size="800x533">
                              <img src="./dist/images/gallery/thumbnails/GC4A3607.jpg" itemprop="thumbnail" alt="Image description" />
                          </a>
                        </figure>

                        <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                          <a href="./dist/images/gallery/GC4A3623.jpg" itemprop="contentUrl" data-size="800x533">
                              <img src="./dist/images/gallery/thumbnails/GC4A3623.jpg" itemprop="thumbnail" alt="Image description" />
                          </a>
                        </figure>

                        <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                          <a href="./dist/images/gallery/GC4A3630.jpg" itemprop="contentUrl" data-size="800x533">
                              <img src="./dist/images/gallery/thumbnails/GC4A3630.jpg" itemprop="thumbnail" alt="Image description" />
                          </a>
                        </figure>


                        <!-- <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                          <a href="./dist/images/gallery/GC4A3646.jpg" itemprop="contentUrl" data-size="800x533">
                              <img src="./dist/images/gallery/thumbnails/GC4A3646.jpg" itemprop="thumbnail" alt="Image description" />
                          </a>
                        </figure> -->

                        <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                          <a href="./dist/images/gallery/GC4A3681.jpg" itemprop="contentUrl" data-size="800x533">
                              <img src="./dist/images/gallery/thumbnails/GC4A3681.jpg" itemprop="thumbnail" alt="Image description" />
                          </a>
                        </figure>

                        <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                          <a href="./dist/images/gallery/GC4A3685.jpg" itemprop="contentUrl" data-size="800x533">
                              <img src="./dist/images/gallery/thumbnails/GC4A3685.jpg" itemprop="thumbnail" alt="Image description" />
                          </a>
                        </figure>

                        <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                          <a href="./dist/images/gallery/GC4A3747.jpg" itemprop="contentUrl" data-size="800x533">
                              <img src="./dist/images/gallery/thumbnails/GC4A3747.jpg" itemprop="thumbnail" alt="Image description" />
                          </a>
                        </figure>



                        <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                          <a href="./dist/images/gallery/GC4A3756.jpg" itemprop="contentUrl" data-size="800x533">
                              <img src="./dist/images/gallery/thumbnails/GC4A3756.jpg" itemprop="thumbnail" alt="Image description" />
                          </a>
                        </figure>

                        <!-- <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                          <a href="./dist/images/gallery/GC4A3774.jpg" itemprop="contentUrl" data-size="800x533">
                              <img src="./dist/images/gallery/thumbnails/GC4A3774.jpg" itemprop="thumbnail" alt="Image description" />
                          </a>
                        </figure> -->

                        <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                          <a href="./dist/images/gallery/GC4A3848.jpg" itemprop="contentUrl" data-size="800x533">
                              <img src="./dist/images/gallery/thumbnails/GC4A3848.jpg" itemprop="thumbnail" alt="Image description" />
                          </a>
                        </figure>

                        <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                          <a href="./dist/images/gallery/GC4A3852.jpg" itemprop="contentUrl" data-size="800x533">
                              <img src="./dist/images/gallery/thumbnails/GC4A3852.jpg" itemprop="thumbnail" alt="Image description" />
                          </a>
                        </figure>


                        <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                          <a href="./dist/images/gallery/GC4A3854.jpg" itemprop="contentUrl" data-size="800x533">
                              <img src="./dist/images/gallery/thumbnails/GC4A3854.jpg" itemprop="thumbnail" alt="Image description" />
                          </a>
                        </figure>

                        <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                          <a href="./dist/images/gallery/GC4A3896.jpg" itemprop="contentUrl" data-size="800x533">
                              <img src="./dist/images/gallery/thumbnails/GC4A3896.jpg" itemprop="thumbnail" alt="Image description" />
                          </a>
                        </figure>

                        <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                          <a href="./dist/images/gallery/GC4A3921.jpg" itemprop="contentUrl" data-size="800x533">
                              <img src="./dist/images/gallery/thumbnails/GC4A3921.jpg" itemprop="thumbnail" alt="Image description" />
                          </a>
                        </figure>

                        <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                          <a href="./dist/images/gallery/GC4A3958.jpg" itemprop="contentUrl" data-size="800x533">
                              <img src="./dist/images/gallery/thumbnails/GC4A3958.jpg" itemprop="thumbnail" alt="Image description" />
                          </a>
                        </figure>


                        <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                          <a href="./dist/images/gallery/GC4A3984.jpg" itemprop="contentUrl" data-size="800x533">
                              <img src="./dist/images/gallery/thumbnails/GC4A3984.jpg" itemprop="thumbnail" alt="Image description" />
                          </a>
                        </figure>

                        <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                          <a href="./dist/images/gallery/GC4A4025.jpg" itemprop="contentUrl" data-size="800x533">
                              <img src="./dist/images/gallery/thumbnails/GC4A4025.jpg" itemprop="thumbnail" alt="Image description" />
                          </a>
                        </figure>

                        <!-- <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                          <a href="./dist/images/gallery/IMG_3032.JPG" itemprop="contentUrl" data-size="800x533">
                              <img src="./dist/images/gallery/thumbnails/IMG_3032.JPG" itemprop="thumbnail" alt="Image description" />
                          </a>
                        </figure> -->

                        <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                          <a href="./dist/images/gallery/IMG_3312.jpg" itemprop="contentUrl" data-size="800x533">
                              <img src="./dist/images/gallery/thumbnails/IMG_3312.jpg" itemprop="thumbnail" alt="Image description" />
                          </a>
                        </figure>


                        <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                          <a href="./dist/images/gallery/IMG_3386.jpg" itemprop="contentUrl" data-size="800x533">
                              <img src="./dist/images/gallery/thumbnails/IMG_3386.jpg" itemprop="thumbnail" alt="Image description" />
                          </a>
                        </figure>

                        <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                          <a href="./dist/images/gallery/IMG_3389.jpg" itemprop="contentUrl" data-size="800x533">
                              <img src="./dist/images/gallery/thumbnails/IMG_3389.jpg" itemprop="thumbnail" alt="Image description" />
                          </a>
                        </figure>

                        <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                          <a href="./dist/images/gallery/IMG_3397.jpg" itemprop="contentUrl" data-size="800x533">
                              <img src="./dist/images/gallery/thumbnails/IMG_3397.jpg" itemprop="thumbnail" alt="Image description" />
                          </a>
                        </figure>

                        <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                          <a href="./dist/images/gallery/IMG_3567.jpg" itemprop="contentUrl" data-size="800x533">
                              <img src="./dist/images/gallery/thumbnails/IMG_3567.jpg" itemprop="thumbnail" alt="Image description" />
                          </a>
                        </figure>

                        <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                          <a href="./dist/images/gallery/IMG_3576.jpg" itemprop="contentUrl" data-size="800x533">
                              <img src="./dist/images/gallery/thumbnails/IMG_3576.jpg" itemprop="thumbnail" alt="Image description" />
                          </a>
                        </figure>

                        <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                          <a href="./dist/images/gallery/IMG_3703.jpg" itemprop="contentUrl" data-size="800x533">
                              <img src="./dist/images/gallery/thumbnails/IMG_3703.jpg" itemprop="thumbnail" alt="Image description" />
                          </a>
                        </figure>

                        <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                          <a href="./dist/images/gallery/IMG_3714.jpg" itemprop="contentUrl" data-size="800x533">
                              <img src="./dist/images/gallery/thumbnails/IMG_3714.jpg" itemprop="thumbnail" alt="Image description" />
                          </a>
                        </figure>


                </div>

                <!-- Root element of PhotoSwipe. Must have class pswp. -->
                <div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">

                    <!-- Background of PhotoSwipe.
                         It's a separate element as animating opacity is faster than rgba(). -->
                    <div class="pswp__bg"></div>

                    <!-- Slides wrapper with overflow:hidden. -->
                    <div class="pswp__scroll-wrap">

                        <!-- Container that holds slides.
                            PhotoSwipe keeps only 3 of them in the DOM to save memory.
                            Don't modify these 3 pswp__item elements, data is added later on. -->
                        <div class="pswp__container">
                            <div class="pswp__item"></div>
                            <div class="pswp__item"></div>
                            <div class="pswp__item"></div>
                        </div>

                        <!-- Default (PhotoSwipeUI_Default) interface on top of sliding area. Can be changed. -->
                        <div class="pswp__ui pswp__ui--hidden">

                            <div class="pswp__top-bar">

                                <!--  Controls are self-explanatory. Order can be changed. -->

                                <div class="pswp__counter"></div>

                                <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>

                                <button class="pswp__button pswp__button--share" title="Share"></button>

                                <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>

                                <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>

                                <!-- Preloader demo http://codepen.io/dimsemenov/pen/yyBWoR -->
                                <!-- element will get class pswp__preloader--active when preloader is running -->
                                <div class="pswp__preloader">
                                    <div class="pswp__preloader__icn">
                                      <div class="pswp__preloader__cut">
                                        <div class="pswp__preloader__donut"></div>
                                      </div>
                                    </div>
                                </div>
                            </div>

                            <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                                <div class="pswp__share-tooltip"></div>
                            </div>

                            <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)">
                            </button>

                            <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)">
                            </button>

                            <div class="pswp__caption">
                                <div class="pswp__caption__center"></div>
                            </div>

                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>

</div>

<?php include("./components/social-feed.php"); ?>

<?php include("./components/footer.php"); ?>

<?php include("./components/foot.php"); ?>
