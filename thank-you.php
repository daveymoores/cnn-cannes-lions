<?php include("./components/head.php"); ?>

<?php include("./components/navigation.php"); ?>

<div class="hero hero--registration">
    <div class="container">
        <div class="row">
            <div class="col-md-7 col-sm-7 col-xs-6">
                <h2>Registration</h2>
            </div>
            <div class="col-md-5 col-sm-5 col-xs-6">
                <img src="dist/images/lions-logo.png" alt="" />
            </div>
        </div>
    </div>
    <div class="hero__panel--blue"></div>
    <div class="hero__panel--body"></div>
</div>


<div class="site-content--wrapper">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="thankyou">
                    <h3>Thank you</h3>
                    <p>You should recieve an email confirmation shortly. If you have any queries please contact <a href="mailTo:CNNEvents@cnn.com">CNNEvents@cnn.com</a>.</p>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include("./components/social-feed.php"); ?>

<?php include("./components/footer.php"); ?>

<?php include("./components/foot.php"); ?>
